require('./bootstrap');

import { createApp } from "vue";

import { fas }from '@fortawesome/free-solid-svg-icons';
import { far }from '@fortawesome/free-regular-svg-icons';
import { fab }from '@fortawesome/free-brands-svg-icons';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas);
library.add(far);
library.add(fab);

const app = createApp({
    components: { FontAwesomeIcon },
});
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => app.component(key.split('/').pop()!.split('.')[0], files(key).default))
app.mount('#app');