@extends('layouts.app')

@section('content')
    <div class="px-4">
        <component-container>
            <h1 class="text-2xl text-center text-black dark:text-white">Tokens</h1>
            <token-manager />
        </component-container>
    </div>
    
    @section('navtab')
    <navbar-tab href={{ url('/home') }}>
        Home
    </navbar-tab>
    @endsection
@endsection
