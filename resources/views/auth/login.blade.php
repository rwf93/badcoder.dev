@extends('layouts.app')

@section('content')
<div class="px-4">
    <component-container>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            
            <h1 class="text-2xl text-center text-black dark:text-white mb-4">Login</h1>

            <!-- Email -->
            <div>
                <div>
                    <input class="shadow-xl appearance-none border rounded w-full py-2 px-3 text-white bg-prim-dark-200 border-prim-dark-300 mb-3" 
                    required
                    placeholder="Email"
                    name="email"
                    id="email" 
                    type="email"
                    value="{{ old('email') }}"
                    >

                    @error('email')
                        <span class="text-red-500 text-sm">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <!-- Password -->
            <div>
                <div>
                    <input class="shadow-xl appearance-none border rounded w-full py-2 px-3 text-white bg-prim-dark-200 border-prim-dark-300 mb-3" 
                    required
                    placeholder="Password"
                    name="password"
                    id="password" 
                    type="password"
                    value="{{ old('password') }}"
                    >

                    @error('password')
                        <span class="text-red-500 text-sm">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <button class="text-white bg-prim-nshades-100 px-8 py-3 rounded mt-4"
                type="submit"
            >
                Login
            </button>
        </form>
    </component-container>
</div>
@endsection