<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@extends('layouts.root')
    <body class='bg-white dark:bg-prim-dark-300 antialiased'>
        <div id="app">            
            <header class="lg:px-16 px-6 bg-gray-300 dark:bg-prim-dark-200 flex flex-wrap items-center lg:py-0 py-2 shadow-xl">
                <div class="flex-1 flex justify-between items-center py-4">
                    <a class="font-sans dark:text-white text-2xl" href={{ url('/') }}> 
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <label for="menu-toggle" class="pointer-cursor lg:hidden block"></label>
                <input class="hidden" type="checkbox" id="menu-toggle" />

                <div class="hidden lg:flex lg:items-center lg:w-auto w-full" id="menu">
                    <nav>
                        <ul class="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                            @auth
                                @yield('navtab')
                                
                                <navbar-dropdown> 
                                    <template v-slot:dropdown-button> <font-awesome-icon :icon="['fas', 'user']" /> </template>
                                    <template v-slot:dropdown-nav> 
                                        <navbar-dropdown-button href={{ route('logout') }} onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </navbar-dropdown-button>
                                    </template>
                                </navbar-dropdown>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            @endauth
                        </ul>
                    </nav>
                </div>
            </header>

            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </body>
</html>
