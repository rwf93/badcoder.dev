<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UploadController;
use Symfony\Component\HttpFoundation\RequestStack;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// uploader routes
Route::post('/uploader/upload', [UploadController::class, 'uploadFile'])->middleware(['auth:api', 
    'scope:upload'
])->name('uploader.upload');

Route::post('/uploader/query', [UploadController::class, 'queryFile']);