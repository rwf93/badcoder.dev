module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node": true
    },
    "overrides" : [
        {
            "files": ["resources/js/**/*.ts", "resources/js/**/*.vue"],
            "extends": [
                "plugin:vue/vue3-recommended",
                '@vue/eslint-config-typescript',
                "plugin:@typescript-eslint/eslint-recommended",
                "plugin:@typescript-eslint/recommended"
            ],
            "parser": "vue-eslint-parser",
            "plugins": [
                "vue",
                "@typescript-eslint"
            ],
            "rules": {
                "indent": "off",
                'vue/html-indent': 'off',
                'vue/html-self-closing': 'off',
                '@typescript-eslint/no-non-null-assertion': 'off',
            }
        }
    ]
}
